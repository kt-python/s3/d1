# [Section] List/Array

names = ["John", "Paul", "Geroge", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]
sample_list = ["Apple", 3, False, 'Potato', 4, True]

# detting list size
print(len(programs))

# accessing values
print(programs[0])
print(programs[-1])
print(names[-3])

print(durations)
print(programs[0:2])

students = ["Mary", "Matthew", "Tom", "Anna", "Thomas"]
grades = [100, 85, 88, 90, 75]

count = 0
while count < len(students):
    print(f"The grade of {students[count]} is { grades[count] }")
    count += 1

# updating lists

print(programs[2])
programs[2] = "Short Courses"
print(f"new value: {programs[2]}")

# [Section] List Manipulation

#append()
programs.append('global')
print(programs)

#delete
durations.append(360)
print(durations)
del durations[-1]
print(durations)

# membership checks
print(20 in durations)
print(500 in durations)

# sorting

print(names)
names.sort()
print(names)

# clear
test_list = [1, 2, 3, 4, 5]
test_list.clear()
print(test_list)

# [Section] Dictionaries/Objects

person1 = {
    "name" : "Brandon",
    "age" : 28,
    "occupation" : "student",
    "isEnrolled" : True,
    "subjects" : ['Python', 'SQL', 'Django']
}

print(len(person1))

print(person1["name"])
print(person1.keys())
print(person1.values())
print(person1.items())

# add key-value pairs
person1["nationality"] = "Filipinpo"
person1.update({"fave_food": "Sinigang"})
print(person1)

# delete key-value pairs
person1.pop("fave_food")
del person1["nationality"]
print(person1)

person2 = {
    "name" : "John"
}
print(person2)
person2.clear()
print(person2)

# looping through a dictionary

for key in person1:
    print(f"The value of {key} is {person1[key]}")

person3 = {
    "name" : "Monika",
    "age" : 20,
    "occupation" : "poet",
    "isEnrolled" : True,
    "subjects" : ['Python', 'SQL', 'Django']
}

classroom = {
    "student1": person1,
    "student2": person3,
}
print(classroom)

car = {
        "brand" : "Toyota",
        "model" : "Vios",
        "year_of_make" : 2015,
        "color" : "Silver"
    }
print(f"I own a {car['brand']} {car['model']}, and it was made in {car['year_of_make']}")

# [Section] Functions

# defines a function called my_greeting
def my_greeting():
	print('Hello User!')


my_greeting()

def greet_user(username): 
	print(f'Hello, {username}!')

greet_user("Bob")
greet_user("Amy")

# From a function's perspective:

def addition(num1, num2):
	return num1 + num2

sum = addition(5, 10)
print(f"The sum is {sum}")

# [Section] Lambda functions
greeting = lambda person : f'hello {person}'
print(greeting("Elsie"))
print(greeting("Anthony"))

mult = lambda a, b : a * b
result = mult(5,6)
print(result)

def sqr(num):
    return num * num

# [Section] Classes
class Car():
    def __init__(self, brand, model, year_of_make):
        self.brand = brand
        self.model = model
        self.year_of_make = year_of_make
        self.fuel = "Gasoline"
        self.fuel_level = 0

    # since we dont have a way to print the entire instance, repr() is used by python developers
    
    # methods
    def fill_fuel(self):
        print(f'Current fuel level: {self.fuel_level}')
        print('filling up the fuel tank...')
        self.fuel_level = 100
        print(f'New fuel level: {self.fuel_level}')
    
    def drive(self, distance):
        print(f"The car has driven {distance} kilometers")
        print(f"The fuel level left: {self.fuel_level - distance}")
        self.fuel_level -= distance

new_car = Car("Nissan", "GT-R", "2019")

# dot notation
print(f"My car is a {new_car.brand} {new_car.model}")

new_car.fill_fuel()

new_car.drive(50)

